const express = require('express');
const bodyParser = require('body-parser');
const db = require('./database.js');  
const bcrypt = require('bcrypt');
const saltRounds = 10;

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.use(express.static('public'));

app.post('/register', (req, res) => {
    const { username, email, password, city, postalCode, region, address } = req.body;

    // Hashing the password
    bcrypt.hash(password, saltRounds, (err, hashedPassword) => {
        if (err) {
            console.error('Error hashing password:', err);
            return res.status(500).send('Server error');
        }

        const postalCodeSql = 'SELECT id FROM postal_code WHERE postal_code = ?';
db.query(postalCodeSql, [postalCode], (err, results) => {
    if (err) {
        console.error('Error checking postal code:', err);
        return res.status(500).send('Server error');
    }

    let postalCodeId;

    if (results.length > 0) {
        // Postal code exists, so use the existing ID
        postalCodeId = results[0].id;
        insertLocation(postalCodeId);
    } else {
        // Postal code doesn't exist, so insert it
        const insertPostalCodeSql = 'INSERT INTO postal_code (postal_code, city) VALUES (?, ?)';
        db.query(insertPostalCodeSql, [postalCode, city], (err, result) => {
            if (err) {
                console.error('Error inserting postal code:', err);
                return res.status(500).send('Server error');
            }
            // Use the new ID from the inserted postal code
            postalCodeId = result.insertId;
            insertLocation(postalCodeId);
        });
    }
});

        function insertLocation(postalCodeId) {
            const locationSql = 'INSERT INTO location (region, address, postal_code_id) VALUES (?, ?, ?)';
            db.query(locationSql, [region, address, postalCodeId], (err, result) => {
                if (err) {
                    console.error('Error inserting location:', err);
                    return res.status(500).send('Server error');
                }
                const locationId = result.insertId;
                registerUser(locationId);
            });
        }

        function registerUser(locationId) {
            const checkUserSql = 'SELECT 1 FROM user WHERE username = ? OR email = ? LIMIT 1';
            db.query(checkUserSql, [username, email], (err, users) => {
                if (err) {
                    console.error('Error checking user:', err);
                    return res.status(500).send('Server error');
                }

                if (users.length > 0) {
                    return res.status(409).send('Username or email already exists');
                }

                const insertUserSql = 'INSERT INTO user (username, email, password, location_id) VALUES (?, ?, ?, ?)';
                db.query(insertUserSql, [username, email, hashedPassword, locationId], (err, results) => {
                    if (err) {
                        console.error('Error saving user:', err);
                        return res.status(500).send('Server error');
                    }
                    res.status(201).send('User registered successfully');
                });
            });
        }
    });
});

app.post('/login', (req, res) => {
    const { email, password } = req.body;

    const sql = 'SELECT * FROM user WHERE email = ?';

    db.query(sql, [email], (err, users) => {
        if (err) {
            console.error('Database error:', err);
            return res.status(500).send('Internal Server Error');
        }

        if (users.length === 0) {
            return res.status(404).send('User not found');
        }

        const user = users[0];

        bcrypt.compare(password, user.password, (err, isMatch) => {
            if (err) {
                console.error('Error comparing passwords:', err);
                return res.status(500).send('Internal Server Error');
            }

            if (isMatch) {
                res.send('Logged in successfully');
            } else {
                res.status(401).send('Incorrect password');
            }
        });
    });
});

const PORT = 5043;
app.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}`);
});
