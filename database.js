const mysql = require('mysql');
const conn = mysql.createConnection({
  host: 'localhost', 
  user: 'root',
  password: '',
  database: 'sisiii2023_ 89211057',
});

conn.connect((err) => {
  if (err) {
    console.error('Error connecting to database:', err);
    return;
  }
  console.log('Connected to the database');
});

module.exports = conn;